%bcond_without sys_llvm
%bcond_with check
%bcond_without toolchain_clang

%if %{with toolchain_clang}
%global toolchain clang
%endif

%global maj_ver 17
%global min_ver 0
%global patch_ver 6
%global bolt_version %{maj_ver}.%{min_ver}.%{patch_ver}
%global bolt_srcdir llvm-project-%{bolt_version}.src

%if %{with sys_llvm}
%global pkg_name llvm-bolt
%global install_prefix %{_prefix}
%else
%global pkg_name llvm-bolt%{maj_ver}
%global install_prefix %{_libdir}/llvm%{maj_ver}
%endif

%global install_bindir %{install_prefix}/bin
%global install_libdir %{install_prefix}/lib
%global install_docdir %{install_prefix}/share/doc
%global max_link_jobs 2

Name:           %{pkg_name}
Version:        %{bolt_version}
Release:        5
Summary:        BOLT is a post-link optimizer developed to speed up large applications
License:        Apache-2.0
URL:            https://github.com/llvm/llvm-project/tree/main/bolt

Source0:        https://github.com/llvm/llvm-project/releases/download/llvmorg-%{bolt_version}/%{bolt_srcdir}.tar.xz
Source1:        https://github.com/llvm/llvm-project/releases/download/llvmorg-%{bolt_version}/%{bolt_srcdir}.tar.xz.sig

Patch1:         0001-Fix-trap-value-for-non-X86.patch
Patch2:         0002-Add-test-for-emitting-trap-value.patch
Patch3:         0003-AArch64-Add-AArch64-support-for-inline.patch
Patch4:         0004-Bolt-Solving-pie-support-issue.patch
Patch5:         0005-BOLT-AArch64-Don-t-change-layout-in-PatchEntries.patch
Patch6:         0006-AArch64-Add-CFG-block-count-correction-optimization.patch
Patch7:         0007-BOLT-Skip-PLT-search-for-zero-value-weak-reference-symbols.patch
Patch8:         0008-merge-fdata-Support-process-no_lbr-profile-file.patch
Patch9:         0009-support-aarch64-instrumentation.patch
Patch10:        0010-AArch64-Add-hybrid-guess-approach-for-edge-weight-estimation.patch
Patch11:        0011-support-D-FOT-addrs-data-parsing-for-optimized-binary.patch

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  cmake
BuildRequires:  ninja-build
BuildRequires:  zlib-devel
BuildRequires:  python3-lit
BuildRequires:  python3-psutil
BuildRequires:  doxygen
%if %{with toolchain_clang}
BuildRequires:	clang
%endif

%description
BOLT is a post-link optimizer developed to speed up large applications.
It achieves the improvements by optimizing application's code layout based
on execution profile gathered by sampling profiler, such as Linux perf tool.

%package doc
Summary: Documentation for BOLT
BuildArch: noarch
Requires: %{name} = %{version}-%{release}
 
%description doc
Documentation for the BOLT optimizer

%prep
%autosetup -n %{bolt_srcdir} -p1

%build
%{cmake} -G Ninja -S llvm \
         -DCMAKE_BUILD_TYPE=RelWithDebInfo \
         -DCMAKE_INSTALL_PREFIX=%{install_prefix} \
         -DCMAKE_SKIP_RPATH=ON \
         -DLLVM_DIR=%{install_libdir}/cmake/llvm \
         -DLLVM_TABLEGEN_EXE=%{install_bindir}/llvm-tblgen \
         -DLLVM_BUILD_UTILS:BOOL=ON \
         -DBOLT_INCLUDE_DOCS:BOOL=ON \
         -DLLVM_INCLUDE_TESTS:BOOL=ON \
         -DBUILD_SHARED_LIBS:BOOL=OFF \
         -DLLVM_LINK_LLVM_DYLIB:BOOL=OFF \
         -DLLVM_ENABLE_ASSERTIONS=ON \
         -DBOLT_INCLUDE_TESTS:BOOL=ON \
         -DLLVM_EXTERNAL_LIT=%{_bindir}/lit \
         -DLLVM_ENABLE_PROJECTS="bolt" \
         -DLLVM_PARALLEL_LINK_JOBS=%{max_link_jobs} \
%if "%{toolchain}" == "clang"
         -DCMAKE_C_COMPILER=clang \
         -DCMAKE_CXX_COMPILER=clang++ \
%endif
%ifarch %ix86 x86_64
         -DLLVM_TARGETS_TO_BUILD="X86"
%endif
%ifarch aarch64
         -DLLVM_TARGETS_TO_BUILD="AArch64"
%endif


# Set LD_LIBRARY_PATH now because we skip rpath generation and the build uses
# some just built libraries.
export LD_LIBRARY_PATH=%{_builddir}/%{bolt_srcdir}/%{__cmake_builddir}/%{_lib}
%cmake_build --target bolt

%install
%cmake_install --component bolt

%ifarch aarch64
find %{buildroot}%{install_prefix} -name "libbolt_rt_hugify.a" -type f,l -exec rm -f '{}' \;
%endif

# Remove files installed during the build phase.
rm -f %{buildroot}/%{_builddir}/%{bolt_srcdir}/%{__cmake_builddir}/%{_lib}/lib*.a

# There currently is not support upstream for building html doc from BOLT
install -d %{buildroot}%{install_docdir}
mv bolt/README.md bolt/docs/*.md %{buildroot}%{install_docdir}

%if %{with check}
%check
%ifarch aarch64
# Failing test cases on aarch64
rm bolt/test/cache+-deprecated.test bolt/test/bolt-icf.test bolt/test/R_ABS.pic.lld.cpp
%endif

export LD_LIBRARY_PATH=%{_builddir}/%{bolt_srcdir}//%{__cmake_builddir}/%{_lib}
export DESTDIR=%{buildroot}
%cmake_build --target check-bolt

# Remove files installed during the check phase.
rm -f %{buildroot}/%{_builddir}/%{bolt_srcdir}/%{__cmake_builddir}/%{_lib}/lib*.a
%endif

%files
%license bolt/LICENSE.TXT
%{install_bindir}/llvm-bolt
%{install_bindir}/llvm-boltdiff
%{install_bindir}/merge-fdata
%{install_bindir}/perf2bolt
%{install_bindir}/llvm-bolt-heatmap

%ifarch aarch64
%{install_libdir}/libbolt_rt_instr.a
%endif

%ifarch x86_64
%{install_libdir}/libbolt_rt_hugify.a
%{install_libdir}/libbolt_rt_instr.a
%endif

%files doc
%doc %{install_docdir}

%changelog
* Tue Feb 11 2025 rfwang07 <wangrufeng5@huawei.com> 17.0.6-5
- Type:backport
- ID:NA
- SUG:NA
- DESC: Sync patch

* Mon Dec 09 2024 Funda Wang <fundawang@yeah.net> - 17.0.6-4
- build with clang

* Tue Nov 12 2024 Funda Wang <fundawang@yeah.net> - 17.0.6-3
- adopt to new cmake macro
- build with gcc now, as llvm/clang will produce linking error
  against libLLVMTableGen.a now

* Fri Jul 5 2024 liyunfei <liyunfei33@huawei.com> - 17.0.6-2
- Add toolchain_clang build support

* Mon Dec 4 2023 zhoujing <zhoujing106@huawei.com> 17.0.6-1
- Update to 17.0.6

* Thu Jun 15 2023 Xiong Zhou <xiongzhou4@huawei.com> 0-2.20211016.gitb72f753
- Type:backport
- ID:NA
- SUG:NA
- DESC: Handle data at the beginning of a function when disassembling and building CFG.

* Mon Dec 19 2022 liyancheng <412998149@qq.com> 0-1.20211016.gitb72f753
- Type:fix
- ID:NA
- SUG:NA
- DESC: Add debuginfo package and delete rpath in binary

* Mon Nov 29 2021 liyancheng <412998149@qq.com>
- Type:Init
- ID:NA
- SUG:NA
- DESC:Init llvm-bolt repository
